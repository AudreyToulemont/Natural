# Natural
A theme for [conky](https://github.com/brndnmtthws/conky). <br />
It displays the **time** and **date**.

## Preview
![Preview](preview.png)

## Installation

Install conky: <br />
Ubuntu: `apt-get install conky-all` <br/>
Fedora: `yum install conky` <br />
Other: see your local documentation or try the Conky documentation.

Clone this repo:
```
git clone git@gitlab.com:AudreyToulemont/Natural.git
```
Copy .conkyrc, and .conky/ to your home directory:
```
cd Natural
cp .conkyrc ~/
cp -r .conky ~/
```
Copy the font to your fonts directory:
```
cp Sawasdee.ttf ~/.fonts
```
Now you are ready to run conky. Open a terminal and run the following:
```
conky -d
```

To run conky at startup, go to System > Preferences > Startup Applications, click "Add" and add the path to the conkyStart file (/usr/share/conkycolors/bin/conkyStart)

## Customization
To edit the colors used in this conky, simply edit *.conkyrc* and change the values of `color0 efb887` and `color1 7ed1d1` with your desired colors.
